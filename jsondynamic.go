package jsondynamic

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

type JsonObj struct {
	Obj interface{}
}
func (j *JsonObj) Pop(path string)  interface{}{
	return JPop(&j.Obj,path)
}
func (j *JsonObj) Del(path string) *JsonObj {
	JDel(&j.Obj,path)
	return j
}
func (j *JsonObj) Set(path string, value interface{}) *JsonObj {
	JSet(&j.Obj, path, value)
	return j
}
func (j *JsonObj) Get(path string) interface{} {
return JGet(j.Obj, path)
}



func (j *JsonObj) BytesIndent() ([]byte,error) {
	return J2bytesIndent(j.Obj)
}

func (j *JsonObj) Byte() ([]byte,error) {
	return J2bytes(j.Obj)
}


func (j *JsonObj) StringIndent() string {
	s,_:=J2StringIndent(j.Obj)
	return s
}


func (j *JsonObj) Append(path string, values ...interface{}) {
	JAppend(&j.Obj, path, values...)
}

func (j *JsonObj) AppendToMe(values ...interface{}) {
	j.Append("", values...)
}

func (j *JsonObj) String() string {
	s,_:=J2String(j.Obj)
	return s
}
func NewJsonObj() *JsonObj {
	return &JsonObj{}
}
func JsonObjFromByte(bs []byte) (JsonObj, error) {
	obj, err := JLoadByte(bs)
	return JsonObj{Obj: obj}, err
}
func JsonObjFromString(str string) (JsonObj, error) {
	obj, err := JLoadString(str)
	return JsonObj{Obj: obj}, err
}

func JsonObjFromObject(obj interface{}) JsonObj {
	return JsonObj{Obj: obj}
}


func JKeys(obj interface{}) []string {
	m, ok := obj.(map[string]interface{})
	a, ok2 := obj.([]interface{})
	if !ok && !ok2 {
		return make([]string, 0)
	}
	if ok {
		rst := make([]string, 0, len(m))
		for k := range m {
			rst = append(rst, k)
		}
		return rst
	}
	rst := make([]string, len(a))
	for i := 0; i < len(a); i++ {
		rst[i] = strconv.Itoa(i)
	}
	return rst
}

func JPath(obj interface{}, path string) interface{} {
	return JGet(obj, path)
}

func JGetString(obj interface{}, path string) string {
	val := JGet(obj, path)
	if val == nil {
		return ""
	}
	return fmt.Sprintf("%v", val)
}

func JGet(obj interface{}, path string) interface{} {
	if path == "" {
		return obj
	}
	m, ok := obj.(map[string]interface{})
	a, ok2 := obj.([]interface{})

	if !ok && !ok2 {
		return nil
	}
	i := strings.IndexRune(path, '.')
	key := path
	newPath := ""
	if i != -1 {
		key = path[:i]
		newPath = path[i+1:]
	}
	if ok {
		return JPath(m[key], newPath)
	}
	idx, err := strconv.Atoi(key)
	if err != nil {
		return nil
	}
	return JPath(a[idx], newPath)
}

func J2StringIndent(obj interface{}) (string, error) {
	bs, err := J2bytesIndent(obj)
	if err != nil {
		return "", err
	}
	return string(bs), nil

}

func J2String(obj interface{}) (string, error) {
	bs, err := J2bytes(obj)
	if err != nil {
		return "", err
	}
	return string(bs), nil

}

func J2bytesIndent(obj interface{}) ([]byte, error) {
	bs, err := json.MarshalIndent(obj, "", " ")
	if err != nil {
		return nil, err
	}
	return bs, nil

}

func J2bytes(obj interface{}) ([]byte, error) {
	bs, err := json.Marshal(obj)
	if err != nil {
		return nil, err
	}
	return bs, nil

}

func JLoadString(str string) (interface{}, error) {
	return JLoadByte([]byte(str))
}

func JLoadByte(bs []byte) (interface{}, error) {
	var obj interface{}
	err := json.Unmarshal(bs, &obj)
	return obj, err
}

func JDel(obj *interface{}, path string) error {
	paths := strings.Split(path, ".")
	key := paths[len(paths)-1]
	paths = paths[:len(paths)-1]
	var o interface{}
	if len(paths) == 0 {
		o = *obj
	} else {
		o = JPath(*obj, strings.Join(paths, "."))
	}
	if mo, ok := o.(map[string]interface{}); ok {
		delete(mo, key)
	} else {
		return errors.New("not a map")
	}
	return nil
}

func JSetArray(obj *interface{}, path string, values ...interface{}) {
	JSet(obj, path, values)
}

func JIsArray(obj interface{}, path string) bool {
	val := JPath(obj, path)
	rVal := reflect.ValueOf(val)
	return rVal.Kind() == reflect.Slice || rVal.Kind() == reflect.Array
}
func JIsObject(obj interface{}, path string) bool {
	val := JPath(obj, path)
	rVal := reflect.ValueOf(val)
	return rVal.Kind() == reflect.Map
}

func JShift(obj *interface{}, path string) interface{} {
	val := JPath(*obj, path)
	if lst, ok := val.([]interface{}); ok {
		if len(lst) == 0 {
			return nil
		}
		rst := lst[0]
		lst = lst[1:]
		JSet(obj, path, lst)
		return rst
	}
	return nil
}

func JPop(obj *interface{}, path string) interface{} {
	val := JPath(*obj, path)
	if lst, ok := val.([]interface{}); ok {
		if len(lst) == 0 {
			return nil
		}
		rst := lst[len(lst)-1]
		lst = lst[:len(lst)-1]
		JSet(obj, path, lst)
		return rst
	}
	return nil
}

func JGetArray(obj interface{}, path string) []interface{} {
	val := JPath(obj, path)
	if val == nil {
		return make([]interface{}, 0)
	}
	if lst, ok := val.([]interface{}); ok {
		return lst
	}
	return []interface{}{val}
}

func JAppend(obj *interface{}, path string, values ...interface{}) {
	lst := JPath(*obj, path)
	if lst == nil {
		lst = make([]interface{}, 0)
	}
	arr, ok := lst.([]interface{})
	if !ok {
		arr = []interface{}{lst}
	}
	lst = append(arr, values...)
	JSet(obj, path, lst)
}

func JSet(obj *interface{}, path string, value interface{}) error {
	if path == "" {
		*obj = value
		return nil
	}
	paths := strings.Split(path, ".")
	o := obj
	for idx, key := range paths {
		oAsMap, okExist := (*o).(map[string]interface{})
		if !okExist {
			*o = make(map[string]interface{}, 0)
			oAsMap = (*o).(map[string]interface{})
		}
		if idx == len(paths)-1 {
			oAsMap[key] = value
			return nil
		}
		nextStep, okExist := oAsMap[key]
		if !okExist {
			nextStep = make(map[string]interface{}, 0)
			oAsMap[key] = nextStep
		}
		_, okIsMap := (nextStep).(map[string]interface{})
		if !okIsMap {
			nextStep = make(map[string]interface{}, 0)
			oAsMap[key] = nextStep
		}
		o = &nextStep
	}
	return nil
}
