package tests

import (
	"encoding/json"
	"fmt"
	"gitlab.com/yehushua.ben.david/jsondynamic"
	"sort"
	"strings"
	"testing"
)

func Test_keys_map(t *testing.T) {
	o := map[string]interface{}{
		"key1": "value",
		"key2": "value",
		"key3": "value",
	}
	keys := jsondynamic.JKeys(o)
	if len(keys) != len(o) {
		t.Errorf("Number of keys is %d should be %d", len(keys), len(o))
		return
	}
	sort.Strings(keys)
	if strings.Join(keys, " ") != "key1 key2 key3" {
		t.Errorf("Keys %v should be [key1 key2 key3]", keys)
		return
	}

}
func Test_IsObject(t *testing.T) {
	var j interface{}
	jsondynamic.JSet(&j,"key","Salut")
	if !jsondynamic.JIsObject(j,"") {
		t.Fatal(j," is an object")
	}
}

func Test_keys_array(t *testing.T) {
	o := []interface{}{100, 101, 102, 103, 104}
	keys := jsondynamic.JKeys(o)
	if len(keys) != len(o) {
		t.Errorf("Number of keys is %d should be %d", len(keys), len(o))
		return
	}
	sort.Strings(keys)
	if strings.Join(keys, " ") != "0 1 2 3 4" {
		t.Errorf("Keys %v should be [0 1 2 3 4]", keys)
		return
	}
}

func Test_array_index(t *testing.T) {
	o := map[string]interface{}{
		"nums": []interface{}{100, 101, 102, 103, 104},
	}
	value := jsondynamic.JPath(o, "nums.1")
	if value.(int) != 101 {
		t.Errorf("%v should be 101", value)
	}
}

func Test_JPath(t *testing.T) {
	jsonBuff := []byte(`{"cars":[{"number":"123XX456","color":"red",  "type":"kia"},
                                {"number":"234YY567","color":"white","type":"suzuki"},
                                {"number":"345ZZ678","color":"black","type":"renault"}]}`)
	var obj interface{}
	json.Unmarshal(jsonBuff, &obj)
	suzuki := jsondynamic.JPath(obj, "cars.1.type")
	if suzuki != "suzuki" {
		t.Errorf("%s should be a suzuki", suzuki)
		return
	}
}

func Test_JDel(t *testing.T) {
	o, err := jsondynamic.JLoadString(`{"data": {"generics":{"french":"toto","english":"foobar"}}}`)
	if err != nil {
		t.Error(err)
		return
	}
	jsondynamic.JDel(&o, "data.generics.french")
	if jsondynamic.JPath(o, "data.generics.french") != nil {
		t.Error("key not deleted !!!")
		return
	}
}
func Test_JSet(t *testing.T) {
	jsonBuff := []byte(`{"data": {"generics":{"french":"toto","english":"foobar"}}}`)
	var obj interface{}
	json.Unmarshal(jsonBuff, &obj)
	jsondynamic.JSet(&obj, "data.generics.hebrew", "stam")
	jsondynamic.JSet(&obj, "data.generics.french", []interface{}{"toto", "titi", "tata"})
	if testValue := fmt.Sprintf("%v", jsondynamic.JPath(obj, "data.generics.english")); testValue != "foobar" {
		t.Errorf("%s should be foobar", testValue)
		return
	}
	if testValue := fmt.Sprintf("%v", jsondynamic.JPath(obj, "data.generics.hebrew")); testValue != "stam" {
		t.Errorf("%s should be stam", testValue)
		return
	}

	if testValue := fmt.Sprintf("%v", jsondynamic.JPath(obj, "data.generics.french.0")); testValue != "toto" {
		t.Errorf("%T: %s should be toto", jsondynamic.JPath(obj, "data.generics.french"), testValue)
		return
	}
	if testValue := fmt.Sprintf("%v", jsondynamic.JPath(obj, "data.generics.french.1")); testValue != "titi" {
		t.Errorf("%s should be titi", testValue)
		return
	}
	if testValue := fmt.Sprintf("%v", jsondynamic.JPath(obj, "data.generics.french.2")); testValue != "tata" {
		t.Errorf("%s should be tata", testValue)
		return
	}
}

func Test_ArrayRoot(t *testing.T) {
	obj, err := jsondynamic.JLoadString(`["a","b","c"]`)
	if err != nil {
		t.Error(err)
		return
	}

	jsondynamic.JAppend(&obj,"","d","e")
	s , _ := jsondynamic.J2String(obj)
	if s != `["a","b","c","d","e"]` {
		t.Error("Root Append Fail")
	}
	jsondynamic.JShift(&obj,"")
	if s,_:=jsondynamic.J2String(obj);s != `["b","c","d","e"]` {
		t.Error("Root Shift Fail")
	}
}

func Test_ArraySet(t *testing.T) {
	obj, err := jsondynamic.JLoadString(`{"scalar":"abc","array":["a","b","c"]}`)
	if err != nil {
		t.Error(err)
		return
	}
	jsondynamic.JSetArray(&obj,"newArray",1,2,3,4,5)
	arr := jsondynamic.JGetArray(obj,"newArray")
	for idx,val := range arr {
		if idx+1 != val  {
			t.Errorf("%v should be [1,2,3,4,5]",jsondynamic.JGetArray(obj,"newArray"))
			return
		}
	}
	jsondynamic.JAppend(&obj,"newArray",6,7,8,9)

	arr = jsondynamic.JGetArray(obj,"newArray")
	for idx,val := range arr {
		if idx+1 != val  {
			t.Errorf("%v should be [1,2,3,4,5,6,7,8,9]",jsondynamic.JGetArray(obj,"newArray"))
			return
		}
	}
	val := jsondynamic.JPop(&obj,"array")
	if val != "c" {
		t.Errorf("%v should be 'c' ",val)
		return
	}
	val = jsondynamic.JShift(&obj,"array")
	if val != "a" {
		t.Errorf("%v should be 'a' ",val)
		return
	}
	cnt := 0
	for v := jsondynamic.JShift(&obj,"newArray");v!=nil;v = jsondynamic.JShift(&obj,"newArray") {
		cnt++
		if v != cnt {
			t.Error("Shifting array Error")
			return
		}
	}
}
func Test_ArrayRead(t *testing.T) {
	obj, err := jsondynamic.JLoadString(`{"scalar":"abc","array":["a","b","c"]}`)
	if err != nil {
		t.Error(err)
		return
	}
	arr := jsondynamic.JGetArray(obj, "scalar")
	if len(arr) != 1 {
		t.Errorf("%v should be ['abc']", arr)
		return
	}
	if arr[0] != "abc" {
		t.Errorf("%v should be ['abc']", arr)
		return
	}
	arr = jsondynamic.JGetArray(obj, "array")
	if len(arr) != 3 {
		t.Errorf("%v should be [a,b,c]", arr)
		return
	}
	if (arr[0] != "a") || (arr[1] != "b") || (arr[2] != "c") {
		t.Errorf("%v should be [a,b,c]", arr)
		return
	}

	isArray := jsondynamic.JIsArray(obj, "array")
	if !isArray {
		t.Errorf("%v should be an array", jsondynamic.JGetArray(obj, "array"))
		return
	}

	isNotArray := jsondynamic.JIsArray(obj, "scalar")
	if isNotArray {
		t.Errorf("%v should not be an array", jsondynamic.JGetArray(obj, "scalar"))
		return
	}

}


