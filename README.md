# jsonDynamic

Golang is strongly Typed but you can still plays with JSON via the map[string]interface{} 


the JsonDynamic package offers you to do the annoying work of casting 


so for exemple :a json like this stored in an obj  as map[string]interface{} 


`{"data":{"type":"product","name":"pencil","stock":{"total":100,"byColor":{"red":40,"blue":60}}}}`


`nbRed := JPath(obj,"data.stock.byColor.red")`
nbRed will be an int as  interface{}  

## License

free to use

